package com.ricardo.dias;


import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

/**
 * Cenário
 * <p>
 * Dado que eu tenho um body do tipo json com as informações necessarias (e-mail e descrição) = corpo
 * Quando eu acesso a url usando o metodo POST
 * Então é retornado o status 200 de SUCESSO
 */

public class TesteApiCadastro extends MassaDeDados {


    @BeforeClass
    public static void urlBase() {
        baseURI = "https://api.thecatapi.com/v1/";
    }

    @Test
    public void cadastroApiCat() {
        Response response =
                given()
                        .contentType("application/json").body(corpoCadastro)
                        .when().post(urlCadastro);
        response.then().body("message", containsString("SUCCESS")).statusCode(200);

        validacao(response);
    }

    @Test
    public void votacaoApiCat() {
        Response response =
                given()
                        .contentType("application/json")
                        .body(corpoVotacao)
                        .header("x-api-key", "live_cqTboXJqRBYzFQXyw21HNg9Q9QdPJQ7YSy2jO5XFapsgkqWRLV03LHaEZWwW2YN6")
                        .when().post("votes/");

        validacao(response);

        String id = response.jsonPath().getString("id");
        vote_id = id;
        System.out.println("ID => " + id);
    }

    @Test
    public void deletaVotacao() {
        votacaoApiCat();
        deletaVoto();
    }

    @Test
    public void FavDel() {
        favoritar();
        desfavoritar();
    }

    private void deletaVoto() {

        String url = "vote/{vote_id}";
        Response response =
                given()
                        .contentType("application/json")
                        .header("x-api-key", "live_cqTboXJqRBYzFQXyw21HNg9Q9QdPJQ7YSy2jO5XFapsgkqWRLV03LHaEZWwW2YN6")
                        .pathParams("vote_id", "vote_id")
                        .when().delete(url);

        validacao(response);
    }

    private void favoritar() {

        Response response =
                given()
                        .contentType("application/json")
                        .body(corpoFavorita)
                        .header("x-api-key", "live_cqTboXJqRBYzFQXyw21HNg9Q9QdPJQ7YSy2jO5XFapsgkqWRLV03LHaEZWwW2YN6")
                        .when().post("favourites/");
        String id = response.jsonPath().getString("id");
        vote_id = id;

        validacao(response);
    }

    private void desfavoritar() {

        Response response =
                given()
                        .contentType("application/json")
                        .header("x-api-key", "live_cqTboXJqRBYzFQXyw21HNg9Q9QdPJQ7YSy2jO5XFapsgkqWRLV03LHaEZWwW2YN6")
                        .pathParams("favourite_id", "vote_id")
                        .when().delete("favourites/{favourite_id}");

        validacao(response);
    }

    public void validacao(Response response) {
        response.then().statusCode(200).body("message", containsString("SUCCESS"));
        System.out.println("RETORNO DESFAVORITAR=> " + response.body().asString());
        System.out.println("-------------------------------------------------------");
    }

}
